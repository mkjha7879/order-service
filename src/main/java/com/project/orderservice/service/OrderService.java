package com.project.orderservice.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.orderservice.dto.OrderLineItemsDto;
import com.project.orderservice.dto.OrderRequest;
import com.project.orderservice.modal.Order;
import com.project.orderservice.modal.OrderLineItems;
import com.project.orderservice.repository.OrderRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;

	public void placeOrder(OrderRequest orderRequest) {
		Order order = new Order();
		order.setOrderNumber(UUID.randomUUID().toString());
		List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDtoList().stream()
				.map(orderLineItemsDto -> mapToOrderLineDto(orderLineItemsDto)).toList();
		order.setOrderLineItems(orderLineItems);
		orderRepository.save(order);
		log.info("Order with id {} is saved into databse", order.getId());
	}

	private OrderLineItems mapToOrderLineDto(OrderLineItemsDto orderlineNumberDto) {
		OrderLineItems items = new OrderLineItems();
		items.setPrice(orderlineNumberDto.getPrice());
		items.setQuantity(orderlineNumberDto.getQuantity());
		items.setSkuCode(orderlineNumberDto.getSkuCode());
		return items;
	}
}
