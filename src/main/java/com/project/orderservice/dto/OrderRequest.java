package com.project.orderservice.dto;

import java.util.List;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {
	private String orderNumber;
	private List<OrderLineItemsDto> orderLineItemsDtoList;
}
